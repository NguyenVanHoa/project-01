<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->truncate();
        for ($i=1; $i < 9; $i++) { 
        DB::table('posts')->insert([
            'title' => 'example_Post_'.$i,
            'description' => 'example_Description_'.$i,
            'content' => 'example_Content_'.$i,
            'status' => true,
            'thumbnail' => 'default_Thumbnail.png',
            'view_count' => '1',
            'category_id' => '1',
            'slug' => 'example-slug-'.$i,
            'user_id' => '1',
            'created_at' => '2020-03-25 01:17:29'
        ]);
        }
    }
}
