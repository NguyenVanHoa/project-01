<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('tags')->truncate();
        for ($i=0; $i < 9; $i++) { 
    	DB::table('tags')->insert([
    		'name' => 'example tag '.$i,
    		'slug' => 'example-slug-'.$i,
            'user_id' => '1',
            'created_at' => '2020-03-25 01:17:29'
    	]);
        }
    }
}
