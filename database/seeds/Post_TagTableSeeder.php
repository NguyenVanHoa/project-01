<?php

use Illuminate\Database\Seeder;

class Post_TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_tag')->truncate();
        for ($i=1; $i < 9; $i++) { 
    	DB::table('post_tag')->insert([
    		'tag_id' => $i,
    		'post_id' => $i,
    	]);
        }
    }
}
