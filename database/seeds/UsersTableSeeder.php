<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        for ($i=1; $i < 4; $i++) { 
        	DB::table('users')->insert([
        		'name'=>'admin'.$i,
	            'email'=>'admin'.$i.'@gmail.com',
	            'password'=>bcrypt('123456'),
	            'phone'=>'123456789',
	            'role'=>1,
                'created_at' => '2020-03-25 01:17:29'
        	]);
        }
    }
}
