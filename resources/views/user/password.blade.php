@extends('layouts.app')



<!-- JavaScripts -->
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection

@section('title')
    change password
@endsection

@section('content')
    <
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width: 550px">
                <div class="card-header">
                    <div>
                        Edit User
                    </div>
                </div>

                <div class="card-body">
                    <!-- Display Validation Errors -->

                    <!-- New Task Form -->
                    <form action="{{route('user.change',Auth::user()->id)}}" method="POST" class="form-horizontal">
                    @csrf
                    @method('PUT')

                    <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm control-label">Password old</label>

                            <div class="col-sm">
                                <input type="password" name="password-old" class="form-control"
                                       value="">
                                @error('password-old')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="task-name" class="col-sm control-label">Password</label>

                            <div class="col-sm">
                                <input type="password" name="password" class="form-control"
                                       value="">
                                @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="task-name" class="col-sm control-label">Password confirm</label>

                            <div class="col-sm">
                                <input type="password" name="password_confirmation" class="form-control"
                                       value="">
                                @error('password_confirmation')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-plus"></i>Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


