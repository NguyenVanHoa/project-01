@extends('layouts.app')
@section('title')
    User
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
    <link rel='stylesheet' type='text/css'
          href='https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css'>
@endsection

<!-- JavaScripts -->
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>--}}
    <script type='text/javascript' src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function dataTable(option = '', key = '',role_option='all') {
                var myTable = $('#listUsers').DataTable({
                    dom: 'lifrtp',
                    processing: true,
                    serverSide: true,
                    searching: true,
                    destroy: true,
                    ajax: {
                        url: route('user.data'),
                        data: {
                            "option": option,
                            "key": key,
                            "role_option":role_option
                        }
                    },
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'phone', name: 'phone'},
                        {data: 'role', name: 'role'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
                });
            }

            dataTable();


            $("#listUsers").on('click', '.edit-modal', function (event) {
                event.preventDefault();
                let user_id = $(this).attr('data-id');
                console.log(user_id);
                $.ajax({
                    type: "get",
                    url: route('user.edit', user_id),
                    success: function (result) {
                        result = JSON.parse(result);
                        $("#name").val(result.name);
                        $("#email").val(result.email);
                        $("#phone").val(result.phone);
                        $("#role").val(result.role);
                        $(".edit-save").attr('data-id', user_id);
                        $("#exampleModal").modal('show');
                    }
                });
            })

            $("#role_option").change(function (event) {
                event.preventDefault();
                var role_option = $("#role_option").val();
                $.ajax({
                    url: route('user.data'),
                    type: "get",
                    data: {
                        "role_option": role_option,
                    },
                    success: function (result) {
                        console.log(result);
                        if (role_option != '')
                            dataTable('','',role_option);
                    }
                })
            })

            $("#search").on('change', function (event) {
                event.preventDefault();
                var option = $("#search-option").val();
                var key = $("#search").val();
                var role_option = $("#role_option").val();
                $.ajax({
                    url: route('user.data'),
                    type: "get",
                    data: {
                        "option": option,
                        "key": key,
                        "role_option": role_option,
                    },
                    success: function (result) {
                        console.log(result);
                        dataTable(option, key, role_option);
                    }
                })
            });

            $(".edit-save").click(function (event) {
                event.preventDefault()
                let user_id = $(this).attr('data-id');
                $.ajax({
                    type: "put",
                    url: route("user.update", user_id),
                    data: {
                        'id': user_id,
                        '_token': $('input[name=_token]').val(),
                        'name': $('input[name=name]').val(),
                        'email': $('input[name=email]').val(),
                        'phone': $('input[name=phone]').val(),
                        'role': $('#role').val(),
                    },
                    success: function (result) {
                        if (result.success) {
                            $('#listUsers').DataTable().ajax.reload();
                            $("#exampleModal").modal("hide");
                            Swal.fire({
                                position: 'center-center',
                                icon: 'success',
                                title: 'Cập nhật thành công',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        } else {
                                if (result.errors.name) {
                                    $('.errorName').show().text(result.errors.name[0])
                                }
                                if (result.errors.email) {
                                    $('.errorEmail').show().text(result.errors.email[0])
                                }
                                if (result.errors.phone) {
                                    $('.errorPhone').show().text(result.errors.phone[0])
                                }
                        }
                    },
                    errors: function (result) {
                        console.log("a");
                    }
                })
            });
            $("#listUsers").on("click", ".btn-delete", function (event) {
                event.preventDefault();
                Swal.fire({
                    title: 'Bạn có muốn xóa?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        let user_id = $(this).attr('data-id');
                        $.ajax({
                            type: "delete",
                            url: route('user.destroy', user_id),
                            success: function (data) {

                                $('#listUsers').DataTable().ajax.reload();

                            }
                        });
                        Swal.fire(
                            'Đã xóa!',
                            'success'
                        )
                    }
                })

            })

            $(".password").click(function (event) {
                event.preventDefault();

                $("#passwordModal").modal('show');
                $(".password_submit").click(function (event) {
                    event.preventDefault();
                    let user_id = $(this).attr('user_id')
                    $.ajax({
                        url: route('user.change', user_id),
                        type: 'put',
                        data: {
                            "password_old": $('input[name=password_old]').val(),
                            "password": $('input[name=password]').val(),
                            "password_confirmation": $('input[name=password_confirmation]').val(),
                        },
                        success: function (result) {
                            console.log(result)
                            if(result.success) {
                                $("#passwordModal").modal('hide');
                                Swal.fire({
                                    position: 'center-center',
                                    icon: 'success',
                                    title: 'Cập nhật thành công',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                            else if(result.errors)
                            {
                                if (result.errors.password_old) {
                                    $('.error_password_old').show().text(result.errors.password_old[0]);
                                }
                                if (result.errors.password) {
                                    $('.error_password').show().text(result.errors.password[0]);
                                }
                                if (result.errors.password_confirmation) {
                                    $('.error_password_confirmation').show().text(result.errors.password_confirmation[0]);
                                }
                            }
                            if(result.error){
                                $('.error_password_old').show().text(result.error);
                            }
                        }
                    })
                })
            })
        });
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Current Tasks -->

            <div class="card">

                <div class="card-header">
                    User List
                </div>
                <div>
                    <a href="" class="btn btn-primary password" user_id="{{Auth::user()->id}}">Đổi mật khẩu</a>
                </div>
                <div class="card-body">
                    <div style="margin-bottom: 2%; width: 45%;display: flex;">
                        <label>Tìm kiếm </label>
                        <select name="search-option" id="search-option" class="form-control">
                            <option value="1">Name</option>
                            <option value="2">Email</option>
                            <option value="3">Phone</option>
                        </select>
                        <input id="search" type="text" placeholder="Nhập key" name="key" class="form-control">
                    </div>
                    <div style="margin-bottom: 2%; width: 45%;display: flex;">
                        <label>Role: </label>
                        <select name="role_option" id="role_option" class="form-control col-sm-3">
                            <option value="all">Tất cả</option>
                            <option value="1">Admin</option>
                            <option value="2">Member</option>
                        </select>
                    </div>
                    <table class="table table-striped task-table" id="listUsers" style="width: 1000px">
                        <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Role</th>
                        <th></th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="" id="#myform">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm">
                                <input type="text" name="name" id="name" class="form-control"
                                       value="">
                                <p style="color: red;" class="error_at_form_edit errorName"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm">
                                <input type="email" name="email" id="email" class="form-control"
                                       value="">
                                <p style="color: red;" class="error_at_form_edit errorEmail"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm">
                                <input type="text" name="phone" id="phone" class="form-control"
                                       value="">
                                <p style="color: red;" class="error_at_form_edit errorPhone"></p>
                            </div>
                        </div>
                        @can('role',Auth::user())
                            <div class="form-group">
                                <label for="task-status" class="col-sm-3 control-label">Role</label>

                                <div class="col-sm">
                                    <select name="role" id="role" class="form-control">
                                        <option value="1">Admin</option>
                                        <option value="0">Member</option>
                                    </select>
                                </div>
                            </div>
                        @endcan
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary edit-save">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Đổi mật khẩu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="task-name" class="col-sm control-label">Password old</label>

                        <div class="col-sm">
                            <input type="password" name="password_old" class="form-control"
                                   value="">
                            <p style="color: red;" class="error_at_form_edit error_password_old"></p>

                        </div>

                    </div>

                    <div class="form-group">
                        <label for="task-name" class="col-sm control-label">Password</label>

                        <div class="col-sm">
                            <input type="password" name="password" class="form-control"
                                   value="">
                            <p style="color: red;" class="error_at_form_edit error_password"></p>

                        </div>

                    </div>

                    <div class="form-group">
                        <label for="task-name" class="col-sm control-label">Password confirm</label>

                        <div class="col-sm">
                            <input type="password" name="password_confirmation" class="form-control"
                                   value="">
                            <p style="color: red;" class="error_at_form_edit error_password_confirmation"></p>

                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary password_submit" user_id="{{Auth::user()->id}}">Save
                        changes
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
