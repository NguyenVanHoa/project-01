@extends('layouts.app')

<!-- JavaScripts -->
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection

@section('title')
    update
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width: 550px">
                <div class="card-header">
                    <div>
                        Edit User
                    </div>
                    @can('password', $user)
                        <div>
                            <form action="{{route('user.password',$user->id)}}" method="GET"
                                  style="margin-right: 5px;">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-edit"></i>Change Password
                                </button>
                            </form>
                        </div>
                    @endcan
                </div>

                <div class="card-body">
                    <!-- Display Validation Errors -->

                    <!-- New Task Form -->
                    <form action="{{route('user.update',$user->id)}}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm">
                                <input type="text" name="name" class="form-control"
                                       value="{{$user->name}}">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>


                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm">
                                <input type="email" name="email" class="form-control"
                                       value="{{$user->email}}">
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Phone</label>

                            <div class="col-sm">
                                <input type="text" name="phone" id="task-deadline" class="form-control"
                                       value="{{$user->phone}}">
                            </div>

                        </div>

                        @can('role',Auth::user())
                            <div class="form-group">
                                <label for="task-status" class="col-sm-3 control-label">Role</label>

                                <div class="col-sm">
                                    <select name="role" id="" class="form-control">
                                        <option value="1" @if($user->role == 1) selected @endif>Admin</option>
                                        <option value="0" @if($user->role == 0) selected @endif>Member</option>
                                    </select>
                                </div>
                            </div>
                    @endif



                    <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-plus"></i>Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

