
<div class="modal fade" id="showTagModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="showTag">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Chi tiết Tag
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <th style="width: 100px">Thuộc tính</th>
                    <th>Thông tin</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Tên thẻ: </td>
                        <td id="name"></td>
                    </tr>
                    <tr>
                        <td>Đường dẫn: </td>
                        <td id="slug"></td>
                    </tr>
                    <tr>
                        <td>Người tạo: </td>
                        <td id="user"></td>
                    </tr>
                    <tr>
                        <td>Ngày tạo: </td>
                        <td id="created_at"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            </form>
        </div>
    </div>
</div>