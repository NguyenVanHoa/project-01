<div class="modal fade" id="addTagModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frmAddTag">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Tạo mới Tag
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="add-error-bag">
                        <ul id="add-tag-errors" style="display: block">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label>
                            Tên Tag
                        </label>
                        <input class="form-control" name="name" required="" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label>
                            Đường dẫn Tag <span style="color: green">(Không bắt buộc)</span>
                        </label>
                        <input class="form-control" name="slug" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-default" data-dismiss="modal" type="button" value="Cancel">
                        <button class="btn btn-info" id="btn-add" type="button" value="add">
                            <i class="fa fa-btn fa-plus"></i>Tạo mới Tag
                        </button>
                    </input>
                </div>
            </form>
        </div>
    </div>
</div>