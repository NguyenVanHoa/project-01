<div class="modal fade" id="editTagModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frmEditTag">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Chỉnh sửa Tag
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-tag-errors" style="display: block">
                        </ul>
                    </div>
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    @if(Session::has('msg_error'))
                    <div class="alert alert-danger">{{ Session::get('msg_error') }}</div>
                    @endif
                    <div class="form-group">
                        <label>
                            Tên Tag
                        </label>
                        <input class="form-control" name="name" required="" type="text">
                        </input>
                    </div>
                    @error('slug')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label>
                            Đường dẫn Tag
                        </label>
                        <input class="form-control" name="slug" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="tag_id" name="tag_id" type="hidden" value="0">
                        <button class="btn btn-info" id="btn-edit" type="button" value="edit">
                            <i class="fa fa-btn fa-plus"></i>Cập nhật Tag
                        </button>
                    </input>
                    </input>
                </div>
            </form>
        </div>
    </div>
</div>