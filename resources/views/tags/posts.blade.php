
@extends('layouts.app')

@section('title')
Danh sách bài viết
@endsection

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
<style>
    body {
        font-family: 'Lato';
    }
    .fa-btn {
        margin-right: 1px;
    }
    .task-table tbody tr td:nth-child(2){
        width: 120px;
    }
    .task-table tbody tr td:nth-child(3){
        width: 100px;
    }
    .add-mb{
        margin-bottom: 2%;
    }
    .swal-title{
        font-family: auto;
    }
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    @if(Session::has('msg'))

    $(function msg(){
        swal("{{ Session::get('msg') }}", " ", "success");
    })

@endif
</script>
@endsection

@section('header')
@include('layouts.header')
@endsection


@section('content')

<div class="container">
    <div class="row justify-content-center" style="display: block;width: 65%;margin: auto;">

        <!-- Current Tasks -->
        <a href="{{ route('tag.index') }}" class="btn btn-success add-mb">
            <i class="fa fa-arrow-left" aria-hidden="true"></i> Danh sách Tag
        </a>
        <div class="card">
            <div class="card-header">
                Bài viết của Tag "{{ $tag->name }}"
            </div>

            <div class="card-body">
                <table class="table table-striped task-table">
                    <thead>
                    <th>STT</th>
                    <th>Tên Tag</th>
                    <th colspan="4">Thao tác</th>
                    </thead>
                    <tbody>
                    @foreach($posts as $key => $post)
                        <tr>
                            <td>{{$key+1}} </td>
                        <td class="table-text">
                            <div><a href="{{ route('post.show',$post->id) }}">
                                {{$post->title}}
                            </a></div>
                        </td>
                        
                        <td>
                            <a href="{{ route('post.show' , $post->id) }}" class="btn btn-success" style="width: 140px;">
                                <i class="fa fa-btn fa-eye"></i> Xem chi tiết
                            </a>
                        </td>
                        
                        <td>
                            <a href="{{ route('post.edit' , $post->id) }}" type="submit" class="btn btn-warning">
                                <i class="fa fa-btn fa-pencil"></i> Chỉnh sửa
                            </a>
                        </td>
                        <!-- Task Delete Button -->
                        <td>
                            <form action="{{ route('post.destroy' , $post->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i> Xóa
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


