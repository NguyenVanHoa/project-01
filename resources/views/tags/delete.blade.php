<div class="modal fade" id="deleteTagModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frmDeleteTag">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete-title" name="title">
                        Xóa Tag
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Bạn có chắc chắn muốn xóa tag này?
                    </p>
                    <p class="text-warning">
                        <small style="color: red">
                            Thao tác này không thể hoàn lại.
                        </small>
                    </p>
                </div>
                <div class="modal-footer">
                    <input id="tag_id" name="tag_id" type="hidden" value="0">
                        <input class="btn btn-default" data-dismiss="modal" type="button" value="Hủy bỏ">
                            <button class="btn btn-danger" id="btn-delete" type="button">
                                Xóa Tag
                            </button>
                        </input>
                    </input>
                </div>
            </form>
        </div>
    </div>
</div>