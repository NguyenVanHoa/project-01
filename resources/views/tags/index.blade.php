
@extends('layouts.app')

@section('title')
Danh sách Tag
@endsection

@section('css')

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<style>
    body {
        font-family: 'Lato';
    }
    .fa-btn {
        margin-right: 1px;
    }
    .task-table tbody tr td:nth-child(2){
        width: 120px;
    }
    .task-table tbody tr td:nth-child(3){
        width: 100px;
    }
    .add-mb{
        margin-bottom: 2%;
    }
    .swal-title{
        font-family: auto;
    }
    .fade.in {
        opacity: 1;
    }
    .modal.fade .modal-dialog {
        transform: translate(0, 0px);
    }
    .modal.fade{
    }
    .modal-backdrop.in {
        filter: alpha(opacity=50);
        opacity: .5;
    }
</style>
@endsection

@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    @if(Session::has('msg'))

    $(function msg(){
        swal("{{ Session::get('msg') }}", " ", "success");
    })

    @endif
</script>
<script type="text/javascript" src="{{ asset('js/tag.js') }}"></script>
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
$(function(){
    function dataTable(name = '' , slug = ''){
        var myTable = $('#tags-table').DataTable({
            processing: true,
            serverSide: true,
            searching:true,
            destroy:true,
            ajax: {
                "url"  : '{!! route('tag.data') !!}',
                "data" : {
                    "name" : name,
                    "slug" : slug,
                },
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'slug', name: 'slug' },
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        })
    }
    dataTable();
    $('.search').on('change', function(e){
      e.preventDefault();
      var name = $('#search_name').val();
      var slug = $('#search_slug').val();
        $.ajax({
            url: '{!! route('tag.data') !!}',
            data: {
                name : name,
                slug : slug,
            },
            success: function (response) {
                dataTable(name, slug);
            },
            error: function (error) {
                console.log('error');
            }
        })
    })
    $("#btn-add").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/tags',
            data: {
                name: $("#frmAddTag input[name=name]").val(),
                slug: $("#frmAddTag input[name=slug]").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmAddTag').trigger("reset");
                $("#frmAddTag .close").click();
                dataTable();
                swal("Tạo mới thành công", " ", "success");
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-tag-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#add-tag-errors').append('<li>' + value + '</li>');
                });
                $("#add-error-bag").show();
            }
        });
    });
    $("#btn-edit").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'PUT',
            url: '/tags/' + $("#frmEditTag input[name=tag_id]").val(),
            data: {
                name: $("#frmEditTag input[name=name]").val(),
                slug: $("#frmEditTag input[name=slug]").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmEditTag').trigger("reset");
                $("#frmEditTag .close").click();
                dataTable();
                swal("Cập nhật thành công", " ", "success");
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#edit-tag-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#edit-tag-errors').append('<li>' + value + '</li>');
                });
                $("#edit-error-bag").show();
            }
        });
    });
    $("#btn-delete").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/tags/' + $("#frmDeleteTag input[name=tag_id]").val(),
            dataType: 'json',
            success: function(data) {
                $("#frmDeleteTag .close").click();
                dataTable();
                swal("Xóa thành công", " ", "success");
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
})
</script>
@endsection

@section('header')
@include('layouts.header')
@endsection


@section('content')

<div class="container">
    <div class="row justify-content-center" style="display: block;width: 65%;margin: auto;">

        <!-- Current Tasks -->
        <a  onclick="event.preventDefault();addTagForm();" href="#" class="btn btn-success add-mb" data-toggle="modal">
            <i class="fa fa-plus"> </i> Thêm mới Tag
        </a>
        <div class="card">
            <div class="card-header">
                Danh sách Tag
            </div>

            <div class="card-body">
                <table class="table table-striped tag-table" id="tags-table">
                    <thead>
            <tr>
                <th style="padding-bottom: 25px;">Id</th>
                <th>Tên thẻ <input type="text" class="search" id="search_name" style="width: 125px" placeholder="Lọc theo tên"></th>
                <th>Đường dẫn <input type="text" class="search" id="search_slug" style="width: 125px" placeholder="Lọc theo slug"></th>
                <th style="padding-bottom: 25px;">Thời gian tạo</th>
                <th style="padding-bottom: 25px;">Thao tác</th>
            </tr>
        </thead>
                </table>
            </div>
        </div>
    </div>
    @include('tags.show')
    @include('tags.create')
    @include('tags.edit')
    @include('tags.delete')
</div>
@endsection


