@extends('layouts.app')

@section('title')
Danh sách danh mục
@endsection

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
    body {
        font-family: 'Lato';
    }
    .fa-btn {
        margin-right: 1px;
    }
    .task-table tbody tr td:nth-child(2){
        width: 120px;
    }
    .task-table tbody tr td:nth-child(3){
        width: 100px;
    }
    .add-mb{
        margin-bottom: 2%;
    }
    th{
        padding-left: 10px !important;
    }
    label.error {
        color: red;
        margin-top: 5px;
        margin-bottom: 0;
    }
</style>
@endsection

@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function dataTable(name = '' , parent_id = ''){
        var myTable = $('.category-table').DataTable({
            processing: true,
            serverSide: true,
            searching:true,
            destroy:true,
            ajax: {
                "url"  : '{!! route('category.data') !!}',
                "data" : {
                    "name" : name,
                    "parent_id" : parent_id,
                },
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'parent_id', name: 'parent_id' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        })
    }
    dataTable();

    $('.parent_id_search').on('change', function() {
        var value_name_search = $('#search_name').val();
        var value_parent_id_search = $('.parent_id_search option:selected').val();
        console.log(value_parent_id_search);
        $.ajax({
            type: 'get',
            url: 'categories/data',
            data: {
                name:value_name_search,
                parent_id : value_parent_id_search,
            },
            success: function (response) {
                if (value_parent_id_search == '') {
                    dataTable(value_name_search);
                }else{
                    dataTable(value_name_search,value_parent_id_search);
                }
            },
            error: function (error) {
                console.log('error');
            }
        })
    });

    $('#search_name').on('change', function(e){
      e.preventDefault();
      var value_name_search = $('#search_name').val();
      var value_parent_id_search = $('.parent_id_search option:selected').val();
        $.ajax({
            type: 'get',
            url: 'categories/data',
            data: {
                name : value_name_search,
                parent_id : value_parent_id_search,
            },
            success: function (response) {
                dataTable(value_name_search, value_parent_id_search);
            },
            error: function (error) {
                console.log('error');
            }
        })
    })

    $('.category-table').on('click', '.btn-delete', function(e){
        e.preventDefault();
        var crfs_token = $('meta[name = "crfs-token"]').attr('content');
        var url = $(this).attr('href');
        swal({
          title: "Bạn có chắc muốn xóa không?",
          text: "Sau khi xóa, bạn sẽ không thể khôi phục lại!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                url : url,
                type :"POST",
                data : {'_method' : 'DELETE', '_token' : crfs_token},
                success : function(data){
                    dataTable();
                    swal({
                        title : "Xóa thành công",
                        icon : "success",
                        button : "Done",
                    });
                },
                error : function (){
                    swal({
                        title : "Xóa thất bại",
                        icon : "error",
                        type : "error",
                        timer : '1500',
                    })
                }
            })
          }
        });
    });

    $('body').on('click', '.btn-create', function(e){
      e.preventDefault();
      $('.name_category_at_form_create').val('');
      $('.description_category_at_form_create').val('');
      $('.parent_id_category_at_form_create > option[value=0]').attr('selected','selected');
      $('label.error').hide();
      $('.error').hide();
      var url = $(this).attr('href');
        $('#modalCreate').modal('show');
        $.ajax({
            type: 'get',
            url: url,
            success: function (response) {
                var categories = response.categories;
                $(".parent_id_category").text('');
                $(".parent_id_category_at_form_create").append("<option value="+0+">Danh mục cha</option>");
                for (i = 0; i < categories.length; i++) {
                    var id = categories[i].id;
                    var name = categories[i].name;
                    $(".parent_id_category_at_form_create").append("<option value = "+id+"> "+name+"</option>");
                }
                $('.btn-store').attr('data-url','{{ asset('categories/store') }}')
            },
            error: function (error) {

            }
        })
    })

    $('#form-create').validate({
        rules : {
            name : {
                required : true,
                minlength : 3,
                maxlength : 100,
            },
            description : {
                required : true,
                minlength : 3,
                maxlength : 200,
            },
        },
        messages : {
            name : {
                required : 'Tên danh mục không được trống',
                minlength : 'Tên danh mục phải ít nhất 3 kí tự',
                maxlength : 'Tên danh mục nhiều nhất 100 kí tự',
            },
            description : {
                required : 'Mô tả danh mục không được trống',
                minlength : 'Mô tả danh mục phải ít nhất 3 kí tự',
                maxlength : 'Mô tả danh mục nhiều nhất 200 kí tự',
            }
        },
        submitHandler: function (form) {
            var url = $(".btn-store").attr('data-url');
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    name: $('.name_category_at_form_create').val(),
                    parent_id: $('.parent_id_category_at_form_create').val(),
                    description: $('.description_category_at_form_create').val(),
                },
                success: function(response) {

                    if (response.success == true) {
                        dataTable();
                        swal({
                            title : "Tạo mới thành công",
                            icon : "success",
                            button : "Done",
                        });
                        $('#modalCreate').modal('hide');
                    }else{
                        if (response.errors.name != undefined) {
                            $('.errorName').show().text(response.errors.name[0]);
                        }
                        if (response.errors.description != undefined) {
                            $('.errorDescription').show().text(response.errors.name[0]);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal({
                        title : "Tạo mới thất bại",
                        icon : "error",
                        type: 'error',
                        button : "Done",
                    });
                }
            })
        }
    });

    $('.category-table').on('click', '.btn-edit', function(e){
      e.preventDefault();
      $('label.error').hide();
      $('.error_at_form_edit').hide();
      var url = $(this).attr('href');
        $('#modalEdit').modal('show');
        e.preventDefault();
        $.ajax({
            type: 'get',
            url: url,
            success: function (response) {
                var categories = response.categories;
                var category = response.category;
                $(".parent_id_category").text('');
                if(response.category.parent_id == null){
                    $(".parent_id_category").append("<option value="+0+" selected = "+'selected'+">Danh mục cha</option>");
                }else{
                    $(".parent_id_category").append("<option value="+0+">Danh mục cha</option>");

                }
                for (i = 0; i < categories.length; i++) {
                    var id = categories[i].id;
                    var name = categories[i].name;
                    var parent_id = categories[i].parent_id;
                    if (id != response.category.id) {
                        if (category.parent_id == categories[i].id) {
                           $(".parent_id_category").append("<option value = "+id+" selected = "+'selected'+"> "+name+"</option>");
                        }else{
                            $(".parent_id_category").append("<option value = "+id+"> "+name+"</option>");
                        }
                    }
                }
                $(".id_category").val(response.category.id);
                $(".name_category").val(response.category.name);
                $('.description_category').val(response.category.description);
                $('.btn-update').attr('data-url','{{ asset('categories/update') }}/'+response.category.id)
            },
            error: function (error) {

            }
        })
    })

    $('#form-edit').validate({
        rules : {
            name : {
                required : true,
                minlength : 3,
                maxlength : 100,
            },
            description : {
                required : true,
                minlength : 3,
                maxlength : 200,
            },
        },
        messages : {
            name : {
                required : 'Tên danh mục không được trống',
                minlength : 'Tên danh mục phải ít nhất 3 kí tự',
                maxlength : 'Tên danh mục nhiều nhất 100 kí tự',
            },
            description : {
                required : 'Mô tả danh mục không được trống',
                minlength : 'Mô tả danh mục phải ít nhất 3 kí tự',
                maxlength : 'Mô tả danh mục nhiều nhất 200 kí tự',
            }
        },
        submitHandler: function (form) {
            var token = $('meta[name = "crfs-token"]').attr('content');
            var url = $(".btn-update").attr('data-url');
            $.ajax({
                type: 'PUT',
                url: url,
                data: {
                    _token:token,
                    _method:'PUT',
                    id: $('.id_category').val(),
                    name: $('.name_category').val(),
                    parent_id: $('.parent_id_category').val(),
                    description: $('.description_category').val(),
                },
                success: function(response) {
                    if(response.success == true){
                        dataTable();
                        swal({
                            title : "Cập nhật thành công",
                            icon : "success",
                            button : "Done",
                        });
                        $('#modalEdit').modal('hide');
                    }else{
                        console.log(response.success);
                        if (response.errors.name != undefined) {
                            $('.errorName').show().text(response.errors.name[0]);
                        }
                        if (response.errors.description != undefined) {
                            $('.errorDescription').show().text(response.errors.name[0]);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal({
                        title : "Cập nhật thất bại",
                        icon : "error",
                        type: 'error',
                        button : "Done",
                    });
                }
            })
        }
    });
})
</script>
<script>
    $(document).ready(function () {
        $('.category-table').on('click', '.btn-show', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                type: 'get',
                url: url,
                success: function(response) {
                    $('span#name_category').text(response.data.name)
                    if (response.parent_id_category != '') {
                        $('span#parent_id_category').text(response.parent_id_category)
                    }else{
                        $('span#parent_id_category').text('Danh mục cha')
                    }
                    $('span#description_category').text(response.data.description)
                    $('span#created_at_category').text(response.created_at_category)
                    $('span#user_id_category').text(response.category_creator)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //xử lý lỗi tại đây
                }
            })
            $("#show").modal('show');
        });
    });
</script>
@endsection

@section('header')
@include('layouts.header')
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center" style="display: block;width: 75%;margin: auto;">
        <!-- Current Tasks -->
        @can('create',App\Category::class)
        <a href="{{ route('category.create') }}" class="btn btn-success add-mb btn-create" style="margin-bottom: 2%">
                <i class="fa fa-plus"> </i> Thêm mới danh mục
        </a>
        @endcan
        <div class="card">
            <div class="card-header">
                Danh sách danh mục
            </div>

            <div class="card-body">
                <div>
                    <div style="margin-bottom: 2%; width: 45%;display: inline-block;">
                        <label>Tìm kiếm theo tên : </label>
                        <input id="search_name" type="text" placeholder="Nhập tên danh mục ..." name="search_name">
                    </div>
                    <div style="margin-top: 2%;display: inline-block;">
                        <label>Tìm kiếm theo danh mục cha : </label>
                        <select class="parent_id_search">
                                <!-- <option></option> -->
                                <option value="all">Tất cả</option>
                                <option value="0">Danh mục cha</option>
                            @foreach($categories as $cate)
                                <option value="{{$cate->id}}">{{$cate->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <table class="table category-table" style="width: 100%;">
                    <thead>
                    <th>STT</th>
                    <th>Tên danh mục</th>
                    <th>Danh mục chứa</th>
                    <!-- <th>&nbsp;</th> -->
                    <th>Thao tác</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@include('categories.show')
@include('categories.edit')
@include('categories.create')
@endsection
