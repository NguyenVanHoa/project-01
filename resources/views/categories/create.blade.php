<div class="modal fade" id="modalCreate">
    <div class="modal-dialog">
        <div class="modal-content">

            <form action="" id="form-create" method="POST" role="form">
                <div class="modal-header">
                    <h4 class="modal-title">Tạo mới</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Tên danh mục</label>
                        <input type="text" class="form-control name_category_at_form_create" placeholder="Nhập tên danh mục ..." name="name">
                        <p style="color: red" class="error errorName"></p>
                    </div>

                    <div class="form-group">
                        <label for="">Danh mục chứa</label>
                        <select name="parent_id" class="form-control parent_id_category_at_form_create" required="required">
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Mô tả</label>
                        <input type="text" class="form-control description_category_at_form_create" placeholder="Nhập mô tả cho danh mục ..." name="description">
                        <p style="color: red" class="error errorDescription"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary btn-store">Lưu</button>

                </div>
            </form>
        </div>
    </div>
</div>