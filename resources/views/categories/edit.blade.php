<div class="modal fade" id="modalEdit">
    <div class="modal-dialog">
        <div class="modal-content">

            <form action="" id="form-edit" method="POST" role="form">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" class="id_category">
                    <div class="form-group">
                        <label for="">Tên danh mục</label>
                        <input type="text" class="form-control name_category" name="name" style="display: block;">
                        <p style="color: red;" class="error_at_form_edit errorName"></p>
                    </div>

                    <div class="form-group">
                        <label for="">Danh mục chứa</label>
                        <select name="parent_id" class="form-control parent_id_category" required="required">
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Mô tả</label>
                        <input type="text" class="form-control description_category" name="description" style="display: block;">
                        <p style="color: red;" class="error_at_form_edit errorDescription"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary btn-update">Cập nhật</button>

                </div>
            </form>
        </div>
    </div>
</div>