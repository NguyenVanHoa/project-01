<div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color: #28a745">Chi tiết danh mục</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5> <span style="font-weight: bold;"> Tên danh mục : </span></span><span id="name_category"></span></h5>
        <h5> <span style="font-weight: bold;"> Danh mục chứa : </span><span id="parent_id_category"></span></h5>
        <h5> <span style="font-weight: bold;"> Mô tả : </span><span id="description_category"></span></h5>
        <h5> <span style="font-weight: bold;"> Người tạo : </span><span id="user_id_category"></span></h5>
        <h5> <span style="font-weight: bold;"> Thời gian tạo : </span><span id="created_at_category"></span></h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>