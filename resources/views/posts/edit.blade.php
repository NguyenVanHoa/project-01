<div class="modal fade" id="modalEdit"style="padding-top: 200px;background-color: #21252975;">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 900px;right: 175px;">

            <div class="card col-sm-12">
                <div class="card-heading">
                    Chỉnh sửa bài viết

                </div>

                <div class="card-body">
                    <!-- Display Validation Errors -->

                    <!-- New Task Form -->
                    <form  method="POST" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <!-- Task Name -->
                        <div class="form-group row">
                            <label  for="task-name" class="col-sm-2 control-label" style="float: left">Tiêu đề</label>

                            <div class="col-sm-6">
                                <input type="text" name="title" class="form-control title" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label">Mô tả bài viết</label>

                            <div class="col-sm-6">
                                <input type="text" name="description" id="description" class="form-control description" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label">Chọn danh mục</label>
                            <div class="col-sm-6">
                                <select class="categories" name="category_id" class="form-control" required="required">

                                </select>
                            </div>

                        </div>


                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label">Nội dung bài viết</label>

                            <div class="col-sm-9">
                                <textarea  name="content" required="required"></textarea>
                            </div>

                        </div>
                        <div class="form-group row">
                            <img src="" alt="" style="width: 90%; height: auto">
                            <label for="exampleInputFile" class="col-sm-2 control-label" >Hình ảnh mô tả bài viết</label>
                            <div class="input-group col-sm-6">

                                <div class="ccol-sm-6">
                                    <input type="file" name="thumbnail" class="thumbnail" id="thumbnail" value="">
{{--                                    <label class="" for="exampleInputFile">Choose file</label>--}}
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="exampleInputFile" class="col-sm-2 control-label" >Tags</label>
                            <div class="input-group col-sm-6">
                                <div class="custom-file">
                                    <input class="form-tags-2" name="tags" type="text" value="" >
                                </div>
                            </div>
                        </div>
                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="button" class="btn edit_save">
                                    <i class="fa fa-btn fa-plus"></i>Lưu bài viết
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div></div></div>
