@extends('layouts.app')

@section('title')
    Tạo bài viết
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('src/jquery.tagsinput-revisited.css')}}"/>

    <style>

        html {
            height: 100%;
            margin: 0;
        }

        body {
            min-height: 100%;
            /*font-family: sans-serif;*/
            padding: 20px;
            margin: 0;
        }

        label {
            display: block;
            padding: 20px 0 5px 0;
        }
    </style>
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 1px;
        }

        .task-table tbody tr td:nth-child(2) {
            width: 120px;
        }

        .task-table tbody tr td:nth-child(3) {
            width: 100px;
        }

        #view-create {
            margin-top: 5%;
        }

        .add-mb {
            margin-bottom: 2%;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 1px;
        }

        .task-table tbody tr td:nth-child(2) {
            width: 120px;
        }

        .task-table tbody tr td:nth-child(3) {
            width: 100px;
        }

        #view-create {
            margin-top: 5%;
        }

        .add-mb {
            margin-bottom: 2%;
        }
    </style>
@endsection

@section('script')
    @routes
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="{{asset('src/jquery.tagsinput-revisited.js')}}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    {{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{--    <script src="https://github.com/tightenco/ziggy/blob/master/src/js/route.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#summernote').summernote({
                placeholder: 'Enter content....',
                tabsize: 2,
                height: 200,
                minHeight: 100,
                maxHeight: 300,
                focus: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                ],
                popover: {
                    image: [
                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                    link: [
                        ['link', ['linkDialogShow', 'unlink']]
                    ],
                    table: [
                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                    ],
                    air: [
                        ['color', ['color']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['para', ['ul', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']]
                    ]
                },
                codemirror: {
                    theme: 'monokai'
                }
            });
        });
    </script>
    <script type="text/javascript">
        @php($list_tag = \App\Tag::all())
        $(document).ready(function () {
            $('#form-tags-4').tagsInput({
                'autocomplete': {
                    source: [
                        @foreach($list_tag as $tag)
                            '{{$tag->name}}',
                        @endforeach
                    ]
                }
            });
        });
    </script>
    <script>
        // $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var data_posts = $('#listPosts').DataTable({
            dom: 'lifrtp',
            retrieve: true,
            processing: true,
            serverSide: true,
            paging: false,
            searching: true,
            destroy: true,
            ajax: {
                // url: route('post.getData'),
                url: "/posts/getData",
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'view_count', name: 'view_count'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        $('.table-posts').on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var crfs_token = $('meta[name = "crfs-token"]').attr('content');
            var url = $(this).attr('href');
            swal({
                title: "Bạn có chắc muốn xóa không?",
                text: "Sau khi xóa, bạn sẽ không thể khôi phục lại!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                //mở trình duyệt đâu đấy
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {'_method': 'DELETE', '_token': crfs_token},
                            success: function (data) {
                                data_posts.ajax.reload(null, false);
                                swal({
                                    title: "Xóa thành công",
                                    icon: "success",
                                });
                            },
                            error: function () {
                                swal({
                                    title: "Xóa thất bại",
                                    icon: "error",
                                    type: "error",
                                    timer: '1500',
                                })
                            }
                        })
                    }
                });
        });
        //create

        $('body').on('click', '.btn-create', function (e) {
            e.preventDefault();
            $('#modalCreate').modal('show');
            $.ajax({
                type: 'get',
                url: 'http://127.0.0.1:8000/posts/create',
                success: function (response) {
                    //js thêm $ vào chi
                    console.log(response)
                    var list_category = response.list_category;
                    // console.log($list_category);
                    for (i = 0; i < list_category.length; i++) {
                        var id = list_category[i].id;
                        var name = list_category[i].name;
                        $(".categories").append("<option class=\"form-control\" value = " + id + "> " + name + "</option>");
                    }
                },
                error: function (error) {
                }
            })
        })
        //save create

        $(".create_save").click(function (event) {
            event.preventDefault();
            // Xử lý  Lấy dữ liệu
            // Xử lý hình ảnh
            let formData = new FormData();
            // console.log($('#thumbnail')[0].files[0])
            formData.append('title', $("input[name='title']").val());
            formData.append('description', $("input[name='description']").val());
            formData.append('category_id', $('.categories').val());
            formData.append('content', $("#summernote").val());
            formData.append('tags', $("input[name='tags']").val());
            formData.append('thumbnail', $('#thumbnail')[0].files[0]);
            console.log(formData.get('thumbnail'));

            $.ajax({
                url: 'http://127.0.0.1:8000/posts/store',
                type: "post",
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success: function (result) {
                    console.log(result)
                    $('#modalCreate').modal('hide')
                    $('#listPosts').DataTable().ajax.reload();
                }
            });

        })

        //edit
        $('.table-posts').on('click', '.btn-edit', function (e) {
            e.preventDefault();
            var post_id = $(this).attr('data-id');
            $('#modalEdit').modal('show');
            $.ajax({
                type: 'get',
                url: 'http://127.0.0.1:8000/posts/'+post_id+'/edit/',
                success: function (response) {
                    //js thêm $ vào chi
                    console.log(response)
                    var post=response.post;
                    console.log(post.title);
                    var list_category = response.list_category;
                    // console.log($list_category);
                    for (i = 0; i < list_category.length; i++) {
                        var id = list_category[i].id;
                        var name = list_category[i].name;
                        if (id==post.category_id)
                        {
                            $(".categories").append("<option selected value = " + id + "> " + name + "</option>");
                        }else{
                            $(".categories").append("<option  value = " + id + "> " + name + "</option>");
                        }

                    }
                    $('.title').val(post.title);
                    $('.description').val(post.description);
                    $('#form-tags-2').val(post.tags);
                    console.log(post.tags);
                    $('#summernote').val(post.content);





                },
                error: function (error) {
                }
            })
        })
        // data_posts;

        $(".edit_save").click(function (event) {
            event.preventDefault();
            var post_id = $(this).attr('data-id');
            console.log(post_id);
            // Xử lý  Lấy dữ liệu
            // Xử lý hình ảnh
            let formData = new FormData();
            // console.log($('#thumbnail')[0].files[0])
            formData.append('title', $("input[name='title']").val());
            formData.append('description', $("input[name='description']").val());
            formData.append('category_id', $('.categories').val());
            formData.append('content', $("#summernote").val());
            formData.append('tags', $("input[name='tags']").val());
            formData.append('thumbnail', $('#thumbnail')[0].files[0]);
            // console.log(formData.get('thumbnail'));

            $.ajax({
                url: 'http://127.0.0.1:8000/posts/udate/'+post_id,
                type: "post",
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success: function (result) {
                    console.log(result)
                    $('#modalEdit').modal('hide')
                    $('#listPosts').DataTable().ajax.reload();
                }
            });

        })

        // });
    </script>
    {{-- create dau --}}
    <script>
        $(document).ready(function () {
            $('.table-posts').on('click', '.btn-show', function (e) {
                e.preventDefault();
                var post_id = $(this).attr('data-id');
                $.ajax({
                    type: 'get',
                    url: route('post.show', post_id),
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response)
                        $('.image').attr('src', 'images/' + response.thumbnail);
                        $('.title').text(response.title);
                        $('.time').text(response.created_at);
                        $('.description').text(response.description);
                        $('.content').html(response.content);
                        $('.user').text(response.user_id);
                        $('.tags').text(response.tags);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //xử lý lỗi tại đây
                    }
                })
                $("#show").modal('show');
            });
        });
    </script>
@endsection
@section('content')
    <div class="container">
        <div class=" row justify-content-center">
            <div class="col-sm-offset-2 col-sm-12 ">
                <a href="{{route('post.create')}}" class="btn btn-success add-mb btn-create">
                    <i class="fa fa-plus"> </i> Thêm mới bai viet
                </a>
                <!-- Current Tasks -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Danh sách bai viet
                    </div>

                    <div class="panel-body">
                        <table class="table table-posts" id="listPosts">
                            <thead>
                            <th>STT</th>
                            <th>Title</th>
                            <th>view_count</th>
                            <th>creat</th>
                            <th>update</th>
                            <th>acction</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('posts.show')
        @include('posts.edit')
    @include('posts.create')
@endsection

{{--<div class="shadow p-3 mb-5 bg-white rounded">--}}
{{--<div class="col-sm-offset-2 col-sm-8 " id="view-create">--}}
{{--    <div class="card">--}}
{{--    <div class="card-header" style="font-size: 20px">--}}
{{--        List Post--}}
{{--    </div>--}}
{{--    <table class="table">--}}
{{--        <thead>--}}
{{--        <tr>--}}
{{--            <th scope="col">#</th>--}}
{{--            <th scope="col">title</th>--}}
{{--            <th scope="col">desctiption</th>--}}
{{--            <th scope="col">#</th>--}}
{{--        </tr>--}}
{{--        </thead>--}}
{{--        <tbody>--}}
{{--        @foreach($list_post as $post)--}}
{{--        <tr>--}}
{{--            <th scope="row">{{$post->id}}</th>--}}
{{--            <td>{{$post->title}}</td>--}}
{{--            <td>{{$post->description}}</td>--}}
{{--            <td><button><a href="{{route('post.destroy',$post->id)}}">delete</a></button></td>--}}
{{--        </tr>--}}
{{--        @endforeach--}}
{{--        </tbody>--}}
{{--    </table>--}}

{{--            <!-- Add Task Button -->--}}

{{--    </div>--}}
{{--        </div>--}}
{{--</div>--}}
