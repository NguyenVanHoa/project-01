@extends('layouts.app')

@section('title')
    Tạo bài viết
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('src/jquery.tagsinput-revisited.css')}}" />

    <style>

        html {
            height: 100%;
            margin: 0;
        }

        body {
            min-height: 100%;
            /*font-family: sans-serif;*/
            padding: 20px;
            margin: 0;
        }
        /*.tag {*/
        /*    position: relative;*/
        /*    background: #556270;*/
        /*    display: block;*/
        /*    max-width: 100%;*/
        /*    word-wrap: break-word;*/
        /*    color: #fff;*/
        /*    padding: 5px 30px 5px 5px;*/
        /*    border-radius: 2px;*/
        /*    margin: 0 5px 5px 0;*/
        /*}*/

        label {
            display: block;
            padding: 20px 0 5px 0;
        }
    </style>
    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 1px;
        }
        .task-table tbody tr td:nth-child(2){
            width: 120px;
        }
        .task-table tbody tr td:nth-child(3){
            width: 100px;
        }
        #view-create{
            margin-top: 5%;
        }
        .add-mb{
            margin-bottom: 2%;
        }
    </style>
@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="{{asset('src/jquery.tagsinput-revisited.js')}}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter content....',
                tabsize: 2,
                height: 200,
                minHeight: 100,
                maxHeight: 300,
                focus: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                ],
                popover: {
                    image: [
                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                    link: [
                        ['link', ['linkDialogShow', 'unlink']]
                    ],
                    table: [
                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                    ],
                    air: [
                        ['color', ['color']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['para', ['ul', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']]
                    ]
                },
                codemirror: {
                    theme: 'monokai'
                }
            });
        });
    </script>
    <script type="text/javascript">
        // @php($list_tag = \Illuminate\Support\Facades\DB::table('post_tag')->where('post_id', $post->id)->get())
        $(document).ready(function() {
            $('#form-tags-4').tagsInput({
                'autocomplete': {
                    source: [
                        @foreach($list_tag as $tag)
                        '{{$tag->name}}',
                        @endforeach
                    ]
                }
            });
        });
    </script>
@endsection



@section('content')
    <div class="container"><a href="{{ route('post.index') }}" class="btn btn-success add-mb" style="    margin-left: 128px !important;">
            <i class="fa fa-arrow-left"> </i> Danh sách bài viết
        </a>
<div class="row justify-content-center" id="view-create">

    <div class="card col-sm-9">
        <div class="card-heading">
            Thêm mới bài viết

        </div>

        <div class="card-body">
            <!-- Display Validation Errors -->

            <!-- New Task Form -->
            <form action="{{ route('post.store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}

            <!-- Task Name -->
                <div class="form-group row">
                    <label for="task-name" class="col-sm-3 control-label" style="float: left">Tiêu đề</label>

                    <div class="col-sm-6">
                        <input type="text" name="title" id="task-name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="task-name" class="col-sm-3 control-label">Mô tả bài viết</label>

                    <div class="col-sm-6">
                        <input type="text" name="description" id="task-name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="task-name" class="col-sm-3 control-label">Chọn danh mục</label>
                    <div class="col-sm-6">
                        <select name="category_id" class="form-control categories">
                            @foreach($list_category as $category)
<!--                                <option value="{{$category->id}}">{{$category->name}}</option>-->
                            @endforeach

                        </select>
                    </div>
                    @if($errors->has('parent_id'))
<!--                        <div class="error">{{ $errors->first('parent_id') }}</div>-->
                    @endif
                </div>


                <div class="form-group row">
                    <label for="task-name" class="col-sm-3 control-label">Nội dung bài viết</label>

                    <div class="col-sm-9">
                        <textarea id="summernote" name="content"></textarea>
                    </div>

                </div>
                <div class="form-group row">
                    <label for="exampleInputFile" class="col-sm-3 control-label" >Hình ảnh mô tả bài viết</label>
                    <div class="input-group col-sm-6">
                        <div class="custom-file">
                            <input type="file" name="thumbnail" class="custom-file-input" id="thumbnail" value="">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                    </div>

                </div>
                <div class="form-group row">
                    <label for="exampleInputFile" class="col-sm-3 control-label" >Tags</label>
                    <div class="input-group col-sm-6">
                        <div class="custom-file">
                            <input id="form-tags-4" name="tags" type="text" value="">
                        </div>
                    </div>
                </div>
                <!-- Add Task Button -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-btn fa-plus"></i>Lưu bài viết
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div></div>

    @endsection
