@extends('layouts.app')

@section('title')
    Tạo bài viết
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 1px;
        }
        .task-table tbody tr td:nth-child(2){
            width: 120px;
        }
        .task-table tbody tr td:nth-child(3){
            width: 100px;
        }
        #view-create{
            margin-top: 5%;
        }
        .add-mb{
            margin-bottom: 2%;
        }
    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
<div class="col-sm-offset-2 col-sm-8" id="view-create">
    <a href="{{ route('post.index') }}" class="btn btn-success add-mb">
        <i class="fa fa-arrow-left"> </i> Danh sách bài viết
    </a>
    <div class="panel panel-default">
        <div class="panel-heading">
            <img src="{{asset('images/'.$list_post->thumbnail)}}" alt="" style="width: 100%; height: 300px">
            <h3>{{$list_post->title}}</h3>
            <br>
            <i>{{$category->name}}</i> , {{$list_post->created_at}}
        </div>

        <div class="panel-body">
            <div class="row">
            <h4><b>{{$list_post->description}}</b></h4> <br>
            <p>{!! $list_post->content !!}</p>
            </div>
            <div class="">
            <p>author: <i>{{\App\User::find($list_post->user_id)->name}}</i></p> <br>
            <p>Tag:
                @foreach($list_tag as $tag)
                    #{{\App\Tag::find($tag->tag_id)->name}}
                @endforeach</p>
            </div>
            <!-- Display Validation Errors -->

            <!-- New Task Form -->



            <!-- Add Task Button -->


        </div>
    </div>
</div>
        </div></div>
@endsection
