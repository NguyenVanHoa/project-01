<div class="modal fade" id="modalCreate"style="padding-top: 200px;background-color: #21252975;">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 900px;right: 175px;">

            <div class="card col-sm-12">
                <div class="card-heading">
                    Thêm mới bài viết

                </div>

                <div class="card-body">
                    <!-- Display Validation Errors -->

                    <!-- New Task Form -->
                    <form id="create_form" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!-- Task Name -->
                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label" style="float: left">Tiêu đề</label>

                            <div class="col-sm-9">
                                <input type="text" name="title" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label">Mô tả bài viết</label>

                            <div class="col-sm-9">
                                <input type="text" name="description"  class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label" id="category_id">Chọn danh mục</label>
                            <div class="col-sm-9">
                                <select name="category_id" class="form-control categories custom-select" id="category_id" required="required">

                                </select>
                            </div>

                        </div>


                        <div class="form-group row">
                            <label for="task-name" class="col-sm-2 control-label">Nội dung bài viết</label>

                            <div class="col-sm-9">
                                <textarea id="summernote" name="content"></textarea>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="exampleInputFile" class="col-sm-2 control-label" >Hình ảnh mô tả bài viết</label>
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    <input type="file" name="thumbnail">
{{--                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="exampleInputFile" class="col-sm-2 control-label" >Tags</label>
                            <div class="input-group col-sm-9">
                                <div class="custom-file" style="width: auto!important;">
                                    <input id="form-tags-4" name="tags" type="text" value="" style="width: auto!important;">
                                </div>
                            </div>
                        </div>
                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="button" class="btn btn-default create_save" >
                                    <i class="fa fa-btn fa-plus "></i>Lưu bài viết
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div></div></div>
