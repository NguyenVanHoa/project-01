<?php

namespace App\Providers;

use App\Policies\UserPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Category;
use App\Policies\CategoryPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [


        Category::class => CategoryPolicy::class,
        User::class => UserPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-user', function ($user, $product){
            return $user->id == $product->user_id;
        });
        Gate::define('update-post',function($user, $post){
           return $user->id ==$post->user_id;
        });
        Gate::define('delete-post',function ($user, $post){
            return $user->id==$post->user_id || $user->role==1;
        });
    }
}
