<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestCategory;
use App\Category;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
    	return view('categories.index')->with([
            'categories' => $categories,
        ]);
    }

    public function getDataCategories(Request $request){
            $categories = Category::select('id','name','slug','user_id','parent_id','description','created_at')->orderBy('id','DESC');
            if($request->has('name')){
                $categories = $categories->where('name','like',"%" . $request->get('name') . "%");
            }
            if ($request->has('parent_id') && $request->get('parent_id') != 'all' && $request->get('parent_id') != '') {
                if ( $request->get('parent_id') != 0) {
                    $categories = $categories->where('parent_id',$request->get('parent_id'));
                }
                if($request->get('parent_id') == 0){
                    $categories = $categories->where('parent_id',null);
                }
            }
            return DataTables::of($categories)
            ->editColumn('parent_id', function ($category) {
                foreach (Category::all() as $cate) {
                    if ($category->parent_id == $cate->id) {
                        return $cate->name;
                    }elseif($category->parent_id == null){
                        return 'Danh mục cha';
                    }
                }
            })
            ->addColumn('action', function ($category) {
                return '<a href="'. route('category.show', $category->id) .'" class="btn btn-xs btn-primary btn-show"  style = "margin-right:5px;"><i class="fa fa-eye"></i> Xem chi tiết</a> <a href="'. route('category.edit', $category->id) .'" class="btn btn-xs btn-warning btn-edit" style = "margin-right:5px;"><i class="fa fa-eye"></i> Chỉnh sửa</a> <a href="'. route('category.destroy', $category->id) .'" data-id="' . $category->id . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create(){
    	$categories = Category::all();
    	return response()->json(['categories' => $categories],200);
    }

    public function show($id){
        // $this->authorize('viewAny', Category::class);
    	$category = Category::findOrFail($id);
        $name_categories = Category::withTrashed()->select('id','name')->get();
        $category_creator = $category->user->name;
        $created_at_category = $category->created_at->format('H:i:s | d/m/Y');
        $parent_id_category = '';
        foreach ($name_categories as $cate) {
            if ($category->parent_id == $cate->id) {
                $parent_id_category = $cate->name;
            }
        }
        return response()->json(['data' => $category, 'category_creator' => $category_creator, 'created_at_category' => $created_at_category , 'parent_id_category' => $parent_id_category],200);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),
            [
            'name' => 'bail|required|min:3|max:100|unique:categories,name',
            'description' => 'bail|required|'
            ],
            [
                'required' => ':attribute không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min kí tự',
                'max' => ':attribute Không được lớn hơn :max kí tự',
                'unique' => ':attribute đã tồn tại',
            ],
            [
                'name' => 'Tên danh mục',
                'description' => 'Mô tả danh mục',
            ]
         );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()],200);
        }else{
        	$category = new Category();
        	$category->name = $request->get('name');
            $category->slug = str::slug($request->get('name'));
            if ($request->get('parent_id') != 0) {
                $category->parent_id = $request->get('parent_id');
            }
            $category->description = $request->get('description');
            $category->user_id = Auth::user()->id;
            $category->save();
            return response()->json(['success' => true],200);
        }
    }

    public function edit($id){
    	$category = Category::findOrFail($id);
        $categories = Category::select('id','name')->get();
        return response()->json(['category' => $category, 'categories' => $categories],200);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(),
            [
            'name' => 'bail|required|min:3|max:100|unique:categories,name,'.$request->get('id'),
            'description' => 'bail|required|'
            ],
            [
                'required' => ':attribute không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min kí tự',
                'max' => ':attribute Không được lớn hơn :max kí tự',
                'unique' => ':attribute đã tồn tại',
            ],
            [
                'name' => 'Tên danh mục',
                'description' => 'Mô tả danh mục',
            ]
         );
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()],200);
        }else{
            $category = Category::findOrFail($request->get('id'));
            $category->name = $request->get('name');
            $category->slug = str::slug($request->get('name'));
            if ($request->get('parent_id') != 0) {
                $category->parent_id = $request->get('parent_id');
            }else{
                $category->parent_id = null;
            }
            $category->description = $request->get('description');
            $category->user_id = Auth::user()->id;
            $category->update();
            return response()->json(['success' => true],200);
        }
    }

    public function destroy($id){
    	$category = Category::findOrFail($id);
        // $this->authorize('delete', $category);
    	$category->delete();
    }
}
