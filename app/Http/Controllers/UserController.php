<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->paginate(10);
        return view('user.index')->with('users', $users);
    }

    public function user_list()
    {
        $users = User::query();
        return Datatables::of($users)
            ->editColumn('role', function ($user) {
                return $user->role == 1 ? "Admin" : "Member";
            })
            ->editColumn('phone', function ($user) {
                return $user->phone ? $user->phone : "Đang cập nhật";
            })
            ->addColumn('action', function ($user) {
                if (Auth::user()->role == 1 && Auth::user()->id != $user->id)
                    return '<a href="" data-id="' . $user->id . '" class="btn btn-xs btn-warning edit-modal"><i class="fa fa-edit"></i> eidt</a> <a href="" data-id="' . $user->id . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
                elseif (Auth::user()->id == $user->id)
                    return '<a href="" data-id="' . $user->id . '" class="btn btn-xs btn-warning edit-modal"><i class="fa fa-edit"></i> eidt</a>';

            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return json_encode($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),
            [
                'name' => 'required|min:3|max:50',
                'email' => 'required|email|unique:users,email,' . $id,
                'phone' => 'required'
            ],
            [
                'required' => ':attribute không được để trống',
                'email' => ':attribute không phải email',
                'unique' => ':attribute đã tồn tại',
            ],
            [
                'name' => 'Tên người dùng',
                'phone' => 'Số điện thoại'
            ]
        );

        if ($validate->fails()) {
            return response()->json(['errors' => $validate->errors()], 200);
        }else{
            $user = User::find($id);
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->role = $request->get('role');
            $user->save();
            return response()->json(['success' => true], 200);
        }




    }

    public function data(Request $request)
    {
        $users = User::select('id', 'name', 'email', 'phone', 'role');
        if ($request->get("key")) {
            if ($request->get('option') == 1) {
                $users = $users->where('name', 'like', "%" . $request->get('key') . "%");
            }
            if ($request->get('option') == 2) {
                $users = $users->where('email', 'like', "%" . $request->get('key') . "%");
            }
            if ($request->get('option') == 3) {
                $users = $users->where('phone', 'like', "%" . $request->get('key') . "%");
            }
        }
        if($request->get("role_option") && $request->get("role_option")!="all" && $request->get("role_option")!="") {
            if ( $request->get('role_option') == 1)
                $users = $users->where('role', $request->get('role_option') );
            else $users = $users->where('role', 0);
        }
        return DataTables::of($users)
            ->editColumn('role', function ($user) {
                return $user->role == 1 ? "Admin" : "Member";
            })
            ->editColumn('phone', function ($user) {
                return $user->phone ? $user->phone : "Đang cập nhật";
            })
            ->addColumn('action', function ($user) {
                if (Auth::user()->role == 1 && Auth::user()->id != $user->id)
                    return '<a href="" data-id="' . $user->id . '" class="btn btn-xs btn-warning edit-modal"><i class="fa fa-edit"></i> eidt</a> <a href="" data-id="' . $user->id . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
                elseif (Auth::user()->id == $user->id)
                    return '<a href="" data-id="' . $user->id . '" class="btn btn-xs btn-warning edit-modal"><i class="fa fa-edit"></i> eidt</a>';

            })
            ->rawColumns(['action'])
            ->make();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        die("a");
    }

    public function password($id)
    {
        $user = User::find($id);
        return view('user.password')->with('user', $user);

    }

    public function change(Request $request)
    {
        $validate = Validator::make($request->all(),
            [
                'password_old'=>'required',
                'password'=>'required|confirmed|min:6',
                'password_confirmation'=>'required'
            ],
            [
                'required' => ':attribute không được để trống',
                'confirmed' => ':attribute không khớp',
                'unique' => ':attribute đã tồn tại',
                'min'=>':attribute phải lớn hơn 6 kí tự'
            ],
            [
                'password-_old'=>'Mật khẩu cũ',
                'password' => 'Mật khẩu mới',
                'password_confirmation'=>'Xác nhận mật khẩu'
            ]
        );

        if ($validate->fails()) {
            return response()->json(['errors' => $validate->errors()], 200);
        }

        $user = Auth::user();
        if (Hash::check($request->get('password_old'), $user->password)) {
            $user->password = Hash::make($request->get('password'));
            $user->save();
            return response()->json(['success' => true], 200);
        } else{
            return response()->json(['error' => 'Mật khẩu cũ không đúng'], 200);
        }
    }
}
