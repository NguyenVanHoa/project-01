<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTagRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use App\Tag;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderByRaw('created_at DESC')->get();
        return view('tags.index')->with([
            'tags'=>$tags,
            'categories'=> \App\Category::all()
        ]);
    }

    public function getIndex()
    {
        
        $now = strtotime(Carbon::now('Asia/Ho_Chi_Minh'));
        $created_at = strtotime(\DB::table('tags')->where('id','=',1)->value('created_at'));
        echo Carbon::now('Asia/Ho_Chi_Minh')."<br>";
        echo \DB::table('tags')->where('id','=',1)->value('created_at')."<br>";
        echo "$now <br>";
        echo "$created_at <br>";
        $time = $now - $created_at;
        // if (floor($time/3600)>1) {
        //     $hour = 0;
        //     $time = ($time/3600)>floor($time/3600+0.5)?
        // }
        echo $this->time(\DB::table('tags')->where('id','=',1)->value('created_at'));
    }

    public function time($time){
        $now = strtotime(Carbon::now('Asia/Ho_Chi_Minh'));
        $time = strtotime($time);

        $year = $this->roundNumber(($now - $time)/3600/24/365);
        $month = $this->roundNumber(($now - $time)/3600/24/30);
        $week = $this->roundNumber(($now - $time)/3600/24/7);
        $day = $this->roundNumber(($now - $time)/3600/24);
        $hour = $this->roundNumber(($now - $time)/3600);
        $minute = $this->roundNumber(($now - $time)/60)<1?1:$this->roundNumber(($now - $time)/60);
        $string = "Đã tạo từ ";
        if ($year >=1) {
            return $string.$year." năm trước";
        }elseif($month >=1){
            return $string.$month." tháng trước";
        }elseif($week>=1){
            return $string.$week." tuần trước";
        }elseif($day>=1){
            if($day == 1){
                return $string."hôm qua";
            }
            return $string.$day." ngày trước";
        }else{
            return $string.$minute." phút trước";
        }
    }

    public function roundNumber($number){
        if ($number<1) {
            return $number;
        }
        $floor = floor($number);
        $number = $number<($floor+0.5)?$floor:$floor+1;
        return $number;
    }

    public function getData(Request $request)
    {
            $tags = Tag::select('id','name','slug','created_at');
            if($request->has('name')){
                $tags = $tags->where('name','like',"%" . $request->get('name') . "%");
            }
            if($request->has('slug')){
                $tags = $tags->where('slug','like',"%" . $request->get('slug') . "%");
            }
            return DataTables::of($tags)
            ->editColumn('created_at', function ($tag){
                return TagController::time(\DB::table('tags')->where('id','=',$tag->id)->value('created_at'));
            })
            ->addColumn('action', function ($tag) {
                return '<a  onclick="event.preventDefault();showTag('. $tag->id .');" href="#" class="btn btn-success show open-modal" data-toggle="tooltip" title="Xem chi tiết">
                            <i class="fa fa-btn fa-eye"></i>
                        </a>
                        <a  onclick="event.preventDefault();editTagForm('. $tag->id .');" href="#" class="btn btn-warning edit open-modal" data-toggle="tooltip" title="Chỉnh sửa">
                                <i class="fa fa-btn fa-pencil"></i>
                            </a>
                        <a href="'.route('tag.posts' , $tag->id) .'" type="submit" class="btn btn-primary" data-toggle="tooltip" title="Bài viết">
                            <i class="fa fa-clipboard" aria-hidden="true"></i>
                        </a>
                        <a onclick="event.preventDefault();deleteTagForm('. $tag->id .');" href="#" class="delete btn btn-danger" data-toggle="tooltip" title="Xóa"><i class="fa fa-btn fa-trash"></i></a>';
            })
            ->rawColumns(['action'])

            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name'=>'required|min:2',
                'slug'=>'unique:tags'
            ],
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min kí tự',
                'unique'=>':attribute đã tồn tại',
            ],
            [
                'name' => 'Tên Tag',
                'slug' => 'Đường dẫn Tag',
            ]
        );
        if($request->get('name')!=''){
        $status = Tag::where('name', 'like binary', $request->get('name'))->get();

        if (!count($status) ==null) {
        $validator->after(function ($validator) {
            $validator->errors()->add('name','Tên Tag đã tồn tại');
        });
        }

        }
        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }
        $tag = new Tag();
        $tag->name = $request->get('name');

        $tag->user_id = Auth::user()->id; 

        $tag->save();
        $tag->slug = $request->get('slug')==null?Str::slug($request->get('name').'-'.$tag->id):Str::slug($request->get('slug'));
        $tag->save();
        Session::flash('msg','Tạo mới Tag '.$tag->name.' thành công');
        return response()->json([
            'error' => false,
            'tag'  => $tag,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $tag = Tag::findOrFail($id);
        $created_at = date('H:i:s d-m-Y',strtotime($tag->created_at));
        $user = \DB::table('users')->where('id',$tag->user_id)->value('name');
        return response()->json([
            'error' => false,
            'tag'  => $tag,
            'created_at'=>$created_at,
            'user'=>$user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('tags.edit')->with([
            'tag'=>$tag,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
            [
                'name'=>'required|min:2',
                'slug'=>Rule::unique('tags')->ignore($id),
            ],
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min kí tự',
                'unique'=>':attribute đã tồn tại',
            ],
            [
                'name' => 'Tên Tag',
                'slug' => 'Đường dẫn Tag',
            ]
        );
        if($request->get('name')!=''){

        $status = Tag::where('name', 'like binary', $request->get('name'))->where('id','!=',$request->get('tag_id'))->get();
        
        if (count($status) >0) {
        $validator->after(function ($validator) {
            $validator->errors()->add('name','Tên Tag đã tồn tại');
        });

        }
        }
        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }
        $tag = Tag::findOrFail($id);
        $tag->name = $request->get('name');
        $tag->save();
        $tag->slug = $request->get('slug')==null?Str::slug($request->get('name').'-'.$tag->id):Str::slug($request->get('slug'));
        $tag->save();
        Session::flash('msg','Đã cập nhật Tag '.$tag->name);
        return response()->json([
            'error' => false,
            'tag'  => $tag,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function posts($tagId){
        $tag = Tag::findOrFail($tagId);
        $posts = $tag->posts()->get();
        return view('tags.posts')->with([
            'tag'=>$tag,
            'posts'=>$posts
        ]);
    }

    public function destroy($id)
    {
        $tag = Tag::destroy($id);

        return response()->json([
            'error' => false,
            'tag'  => $tag,
        ], 200);
    }
}
