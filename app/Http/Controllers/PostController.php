<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use App\Post;
use Yajra\DataTables\Facades\DataTables;

class postController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        return DataTables::of(Post::query())->addColumn('action', function ($post) {
            return '<a href="'. route('post.show', $post->id) .'" class="btn btn-xs btn-primary btn-show" data-id = "'.$post->id.'"  style = "margin-right:5px;"><i class="fa fa-eye"></i> Xem chi tiết</a> <a href="'. route('post.edit', $post->id) .'" class="btn btn-xs btn-warning btn-edit" data-id="'.$post->id.'" style = "margin-right:5px;"><i class="fa fa-eye"></i> Chỉnh sửa</a> <a href="'. route('post.destroy', $post->id) .'" data-id="' . $post->id . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
//            return '<a href="' . route('post.show', $post->id) . '" class="btn btn-xs btn-warning">show</a><a href="' . route('post.edit', $post->id) . '" class="btn btn-xs btn-warning">edit<a href="" data-id="' . $post->id . '" class="btn btn-xs btn-danger btn-delete"> Delete</a>';
        })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function index()
    {

        $list_posts=Post::orderBy('id','DESC')->get();
        $list_posts=Post::orderBy('id','DESC')->get();
        $list_posts=Post::all()->sortDesc();
//        dd($list_posts);
        return view('posts.index')->with([
            'list_post' => $list_posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()) {
            return redirect('/login');
        }
        $list_category = Category::all();
        $list_stag = Tag::all();
        $tags = '';
        foreach ($list_stag as $tag) $tags .= $tag->name . ',';
//        dd($tags);
        return response()->json([
            'list_category' => $list_category,
            'tags' => $tags,
            'list_tags' => $list_stag
        ],200);
//        return view('posts.create')->with([
//            'list_category' => $list_category,
//            'tags' => $tags,
//            'list_tags' => $list_stag
//        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        var_dump($request->all());
//        die($request->allFiles());
        $post = new Post();
        $thumbnail = $request->file('thumbnail');
        $thumbnail->store('image');
        $name = date('YmdHis') . "." . $thumbnail->getClientOriginalExtension();
        $thumbnail->move('images', $name);
        $post->thumbnail = $name;
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->category_id = 1;
        $post->description = $request->get('description');
        $post->view_count = 0;
        $post->status = 0;
        $post->user_id = Auth::user()->id;
        $post->slug = Str::slug($post->description) . time();

        $tags = explode(',', $request->tags);
        $post->save();
        for ($i = 0; $i < count($tags); $i++) {
            $tag = Tag::all()->where('name', $tags[$i])->first();
            if ($tag == null) {
                //neu tag chua co thi tao tag moi
                $new_tag = new Tag();
                $new_tag->name = $tags[$i];
                $new_tag->slug = Str::slug($tags[$i]) . time();
                $new_tag->user_id = $post->user_id;
                $new_tag->save();
                //sau do thi tao quan he tren bang phu
                \DB::table('post_tag')->insert(
                    ['post_id' => $post->id, 'tag_id' => $new_tag->id]
                );
            } else {
                //tag co san thi tao quan he tren bang phu
                \DB::table('post_tag')->insert(
                    ['post_id' => $post->id, 'tag_id' => $tag->id]
                );
            }
        }
//        if($save) return response()->json('success',true);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if ($post == null) {
            return abort(404);
        }
        $list_tag = \DB::table('post_tag')->where('post_id', $post->id)->get();
//        dd($list_tag);
        return json_encode($post);
//        return view('posts.show')->with([
//            'post' => $post,
//            'category' => Category::find($post->category_id),
//            'list_tag' => $list_tag
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if ($post == null) {
            return abort(404);
        }
        if (Gate::allows('update-post', $post)) {
            //tags la xau tag1,tag2,..tagn
            $list_stag = Tag::all();
            //list_stag la mang gom ta ca cac tag
            //list_tag la mang gom cac tag cua bai viet

            $list_tag = DB::table('post_tag')->where('post_id', $post->id)->get();
            $tags = '';
            foreach ($list_tag as $tag) $tags .= Tag::find($tag->tag_id)->name . ',';
            $tags = trim($tags, ',');

            $list_category = Category::all();

            return response()->json([
                'post' => $post,
                'list_category' => $list_category,
                'list_tag' => $list_tag,
                'list_tags' => $list_stag,
                'tags' => $tags
            ],200);

        } else {
            return abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if ($post == null) {
            return abort(404);
        }

        if (Gate::allows('update-post', $post)) {
            if ($request->thumbnail != null) {
                $thumbnail = $request->file('thumbnail');
                $thumbnail->store('image');
                $name = date('YmdHis') . "." . $thumbnail->getClientOriginalExtension();
                $thumbnail->move('images', $name);
                $post->thumbnail = $name;
            }
            $post->title = $request->get('title');
            $post->content = $request->get('content');
            $post->category_id = $request->get('category_id');
            $post->description = $request->get('description');
            $post->slug = Str::slug($post->description) . time();

            //xoa tat ca cac lien ket voi cac tag ban dau
            DB::table('post_tag')->where('post_id', $post->id)->delete();

            //tao lai lien ket voi cac tag moi
//        dd($request->tags);
            $tags = explode(',', $request->tags);
            $post->save();
            for ($i = 0; $i < count($tags); $i++) {
                $tag = Tag::all()->where('name', $tags[$i])->first();
                if ($tag == null) {
                    //neu tag chua co thi tao tag moi
                    $new_tag = new Tag();
                    $new_tag->name = $tags[$i];
                    $new_tag->slug = Str::slug($tags[$i]) . time();
                    $new_tag->user_id = $post->user_id;
                    $new_tag->save();
                    //sau do thi tao quan he tren bang phu
                    \DB::table('post_tag')->insert(
                        ['post_id' => $post->id, 'tag_id' => $new_tag->id]
                    );
                } else {
                    //tag co san thi tao quan he tren bang phu
                    \DB::table('post_tag')->insert(
                        ['post_id' => $post->id, 'tag_id' => $tag->id]
                    );
                }
            }
            return $this->show($post->id);
        } else return abort(403, 'Unauthorized action.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $post = Post::find($id);
        $post->delete();
        $post=Post::where('id',$id);
        $post->delete();
        return redirect()->back();
        $post=Post::find($id);
        if($post==null){
            return abort(404);
        }
        if(Gate::allows('delete-post',$post)) {
            $post->delete();
            return redirect()->back();
        }else return abort(403, 'Unauthorized action.');
    }
}
