<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        return [
            'name'=> 'required',
            'slug' => Rule::unique('tags')->ignore($id),
        ];
    }
    public function messages(){
        return [
            'required'=>':attribute không được để trống',
            'unique'=>':attribute đã tồn tại',
        ];
    }
    public function attributes(){
        return [
            'name'=>'Tên Tag',
            'slug'=>'Đường dẫn Tag',
        ];
    }
}
