<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'posts';

    protected $fillable = [
    	'title', 'description', 'content', 'status', 'thumbnail', 'view_count', 'category_id', 'slug', 'user_id'
    ];

    public function posts(){
    	return $this->belongsToMany(\App\Tag::class);
    }
}
