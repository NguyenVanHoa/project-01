<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = [
    	'name','slug','user_id',
    ];

    use Notifiable;
    use SoftDeletes;

    public function posts(){
    	return $this->belongsToMany(\App\Post::class);
    }
}
