<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    protected $table = 'categories';
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id')->withTrashed();
    }
}
