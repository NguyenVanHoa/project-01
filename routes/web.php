<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});

Route::group([
    'prefix' => 'categories',
    'middleware' => 'auth',
    'as' => 'category.'
], function () {
    Route::get('', 'CategoryController@index')->name('index');
    Route::get('data', 'CategoryController@getDataCategories')->name('data');
    Route::get('search', 'CategoryController@searchDataCategories')->name('search');
    Route::get('show/{id}', 'CategoryController@show')->name('show');
    Route::get('create', 'CategoryController@create')->name('create');
    Route::post('store', 'CategoryController@store')->name('store');
    Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
    Route::put('update/{id}', 'CategoryController@update')->name('update');
    Route::delete('delete/{id}', 'CategoryController@destroy')->name('destroy');
});


Route::group([
    'prefix' => 'tags',
    'middleware'=>'auth',
    'as' => 'tag.',
],function(){
    route::get('/', 'tagcontroller@index')->name('index'); //hiển thị danh sách tag

    route::get('create', 'tagcontroller@create')->name('create'); //hiển thị form tạo mới 1 tag

    route::post('/', 'tagcontroller@store')->name('store'); //tạo mới tag

    route::get('/show/{id}', 'tagcontroller@show')->name('show'); //hiển thị 1 tag

    route::get('edit/{id}', 'tagcontroller@edit')->name('edit'); //hiển thị form cập nhât 1 tag

    route::get('posts/{id}', 'tagcontroller@posts')->name('posts'); //hiển thị danh sách bài viết


    route::put('/{id}','tagcontroller@update')->name('update'); //cập nhập tag


    route::get('data', 'tagcontroller@getdata')->name('data');


});

Route::group([
    'prefix' => 'posts',
    'as' => 'post.'
], function () {
    Route::get('/', 'PostController@index')->name('index');
    Route::get('create', 'PostController@create')->name('create');
    Route::post('store', 'PostController@store')->name('store');
    Route::get('{id}/edit', 'PostController@edit')->name('edit');
    Route::get('show/{id}', 'PostController@show')->name('show');
    Route::post('update/{id}', 'PostController@update')->name('update');
    Route::delete('delete/{id}', 'PostController@destroy')->name('destroy');
    Route::get('getData', 'PostController@getData')->name('getData');

});


Route::group([
    'middleware' => 'auth',
    'prefix' => 'user',
    'as' => 'user.'
], function () {
    Route::get('/', 'UserController@index')->name('index');
    Route::get('/user-list', 'UserController@user_list')->name('list');
    Route::get('/search', 'UserController@data')->name('data');

    Route::get('create', 'UserController@create')->name('create');
    Route::post('/', 'UserController@store')->name('store');
    Route::put('update/{id}', 'UserController@update')->name('update');
    Route::delete('destroy/{id}', 'UserController@destroy')->name('destroy');
    Route::get('password/{id}', 'UserController@password')->name('password');
    Route::put('change/{id}', 'UserController@change')->name('change');
    Route::get('{id}/edit', 'UserController@edit')->name('edit');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
