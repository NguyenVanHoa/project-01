function showTag(tag_id) {
    $.ajax({
        type: 'GET',
        url: '/tags/show/' + tag_id,
        success: function(data) {
            $("#name").html(data.tag.name);
            $("#slug").html(data.tag.slug);
            $('#user').html(data.user);
            $("#created_at").html(data.created_at);
            $('#showTagModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
    $(document).ready(function() {
        $("#add-error-bag").hide();
        $('#showTagModal').modal('show');
    });
}

function addTagForm() {
    $(document).ready(function() {
        $("#add-error-bag").hide();
        $('#addTagModal').modal('show');
    });
}

function editTagForm(tag_id) {
    $.ajax({
        type: 'GET',
        url: '/tags/show/' + tag_id,
        success: function(data) {
            $("#edit-error-bag").hide();
            $("#frmEditTag input[name=name]").val(data.tag.name);
            $("#frmEditTag input[name=slug]").val(data.tag.slug);
            $("#frmEditTag input[name=tag_id]").val(data.tag.id);
            $('#editTagModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function deleteTagForm(tag_id) {
    $.ajax({
        type: 'GET',
        url: '/tags/show/' + tag_id,
        success: function(data) {
            $("#frmDeleteTag #delete-title").html("Xóa Tag (" + data.tag.name + ")?");
            $("#frmDeleteTag input[name=tag_id]").val(data.tag.id);
            $('#deleteTagModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}